<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>panier PHP</title>
</head>
<body>

 
<?php
/*debut de la session */
session_start();
/* Article */
$select = array();
$select['id'] = "Pomme";
$select['qte'] = 5;

$select['prix'] = 3;

/* je verifie si le panier existe*/
if(!isset($_SESSION['panier']))
{
    /* j'inisialise le panier */
    $_SESSION['panier'] = array();

    $_SESSION['panier']['qte'] = array();
    
    $_SESSION['panier']['prix'] = array();
}

/* j"ajoute  un article dedans. */
array_push($_SESSION['panier']['pomme'],$select['id']);
array_push($_SESSION['panier']['1'],$select['qte']);

array_push($_SESSION['panier']['3'],$select['prix']);

/* jaffiche le contenu du panier */
?>
<li>
<?php
var_dump($_SESSION['panier']);
?>
</li>

<div class="container-fluid">
 <div class="card h-100 flex-row justify-content-around align-items-center mt-2 bg-secondary">
 <img style="width:125px; height:125px" class="img-thumbnail" src="<?php echo $img_fruit?>" alt="<?php echo $nom_fruit?>">
 <div>
 <h4><?php $nom_fruit ?></h4>
 <p>Pour avoir la pêche, mangez des <?php echo $nom_fruit?>s !</p>
 </div>
 <div>
 <form method="POST">
 <label for="<?php echo $id?>">Quantité</label>
 <select name="<?php echo $id?>" id="<?php echo $id?>">
 <option value="0">0</option>
 <option value="1">1</option>
 <option value="2">2</option>
 <option value="3">3</option>
 <option value="4">4</option>
 <option value="5">5</option>
 </select>
 </div>
 <div>
 <h4><?php echo $prix_fruit?>€</4>
 <input type="submit" value="Ajouter">
 </form>
 </div>
 
 </div>
    
</body>
</html>